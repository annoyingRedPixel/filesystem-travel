package filesystem_travel;
import org.jvnet.hk2.annotations.Service;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Service
public class FilesystemTravel {


    public List<String> travel(String rootPath) throws IOException {

        File[] files = new File(rootPath).listFiles();
        var pathList = new ArrayList<String>();

        getFiles(files, pathList,0, Path.of(rootPath));

        return pathList;
    }

    private void getFiles(File[] files, ArrayList<String> pathList, int level, Path rootDir) throws IOException {
        System.out.println(String.format("rootPath:%s, level: %d", rootDir, level));
        for(File filename : files){
            if(filename.isDirectory() ){
                level += 1;
                getFiles(filename.listFiles(), pathList, level, filename.toPath());

            } else {
                if(pathList.contains(filename.getCanonicalPath())){
                    throw new RuntimeException("Loop found!");
                } else {
                    pathList.add(filename.getCanonicalPath());
                }
            }
        }
    }
}
