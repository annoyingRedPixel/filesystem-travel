package filesystem_travel;

import jdk.jfr.Description;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class FilesystemTravelTest {

    @Test
    @Description("Testing the app without the usage of a symbolic link")
    void travel() throws IOException {
        FilesystemTravel filesystemTravelTest1 = new FilesystemTravel();

        List<String> resultList = filesystemTravelTest1.travel("./src/test/resources/TestFilesWithoutSymLink");
        List<String> expectedList = new ArrayList<>();
        for(String result : resultList){
            expectedList.add(result.substring((result.length() - 9)));
        }
        List<String> actualList  = new ArrayList<>(List.of("file1.txt","file2.txt"));

        assertThat(actualList, is(expectedList));
    }

    @Test
    @Description("Testing the app with an symbolic link in the filesystem")
    void travel2() throws IOException {

        FilesystemTravel filesystemTravel1 = new FilesystemTravel();

        RuntimeException thrown = Assertions.assertThrows(RuntimeException.class, () -> {
            filesystemTravel1.travel("./src/test/TestFilesWithSymLink");
        });

        Assertions.assertEquals("Loop found!", thrown.getMessage());
    }


}
